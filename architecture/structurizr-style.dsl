styles {
    element "Person" {
        shape Person
        background #00bfa5
        color #ffffff
        description false
    }
    element "Library" {
        shape Folder
        background #ffffff
        color #071E3C
        stroke #4050c6
        description false
    }
    element "Container" {
        shape Hexagon
        background #52b1ff
        color #ffffff
        description false
    }
    element "Component" {
        shape Component
        background #ffffff
        color #071E3C
        stroke #071E3C
        description false
    }
    element "Software System" {
        shape RoundedBox
        background #4050c6
        color #ffffff
        description false
    }
    element "WebBrowser" {
        shape WebBrowser
        background #ffffff
        color #071E3C
    }
    element "Mobile" {
        shape MobileDevicePortrait
    }
    element "Repository" {
        shape Cylinder
        background #4050c6
        color #ffffff
        description false
    }
    element "MessageBroker" {
        shape Pipe
        background #4050c6
        color #ffffff
        description false
    }
    element "MessageBus" {
        shape Pipe
        background #4050c6
        color #ffffff
        description false
    }
    element "EventBus" {
        shape Pipe
        background #4050c6
        color #ffffff
        description false
    }

    # Estilo por default para relaciones
    relationship "Relationship" {
        color #071E3C
        dashed false
    }
    # Estilo para relaciones síncronas
    relationship "RPC" {
        color #071E3C
        dashed false
    }
    relationship "Sync" {
        color #071E3C
        dashed false
    }
    # Estilo para relaciones asincronas
    relationship "Async" {
        color #00bfa5
        dashed true
    }
    relationship "Message" {
        color #00bfa5
        dashed true
    }
    relationship "Event" {
        color #00bfa5
        dashed true
    }

}

